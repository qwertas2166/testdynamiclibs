﻿using System.Collections.Generic;

namespace src.nyuro.di {
    public static class Counter {
        
        //this dictionary should use weak key references
        static Dictionary<object, int> DICTIONARY = new();
        static int gid = 0;

        public static int GetId(this object o) {
            if (DICTIONARY.ContainsKey(o)) {
                return DICTIONARY[o];
            }
            gid++;
            return DICTIONARY[o] = gid;
        }
    }
}