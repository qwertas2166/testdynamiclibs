using UnityEngine;

namespace src.nyuro.di {
    public class TestScriptA : MonoBehaviour {

        private TestDependencyB testDependencyB;

        void Start() {
            Debug.Log("DI test");
            Debug.Log(this.GetId());
            Debug.Log(testDependencyB.text);
        }

        public void init(TestDependencyB testDependencyB) {
            Debug.Log(this.GetId());
            this.testDependencyB = testDependencyB;

        }

        void Update() { }
    }
}