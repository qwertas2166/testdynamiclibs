﻿using UnityEngine;

namespace src.nyuro.di {

    namespace src.nyuro.di {
        public class TestService {
            private TestDependencyB testDependencyB;

            public void setTestDependencyB(TestDependencyB testDependencyB) {
                this.testDependencyB = testDependencyB;
                Debug.Log("Set dependencyB to TestService");
            }
        }
    }
}