﻿using System.Linq;
using Autofac;
using src.nyuro.di.src.nyuro.di;
using UnityEngine;

namespace src.nyuro.di {
    public class DependencyInjectionDescription : MonoBehaviour {
        
        void Awake() {
            this.inject();
        }

        public void inject() {
            // MonoBehaivour
            TestScriptA[] testScripts = Object.FindObjectsByType<TestScriptA>(FindObjectsSortMode.None).ToArray();
            foreach (TestScriptA testScriptA in testScripts) {
                testScriptA.init(new TestDependencyB());
            }
            // Service
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterType<TestService>().OnActivating(e => e.Instance.setTestDependencyB(new TestDependencyB()));
            IContainer container = builder.Build();
            container.Resolve<TestService>();
        }
    }
}